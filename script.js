const expresiones = {
    codigo: /^[a-zA-Z0-9]{1,5}$/, // alfanumérico de 5 caracteres.
    marca: /^[a-zA-Z0-9]{1,50}$/, // alfanumérico de 50 caracteres.
    modelo: /^[a-zA-Z0-9]{1,30}$/, // alfanumérico de 30 caracteres.
    año: /^\d{4}$/ // numérico de 4 dígitos.
};

document.querySelector("#btnIn").addEventListener("click", validar);

function validar() {
    
  var cod = document.getElementById("cod").value;
  var mar = document.getElementById("mar").value;
  var mod = document.getElementById("mod").value;
  var año = document.getElementById("año").value;
  var fchini = document.getElementById("fchini").value;
  var fchfin = document.getElementById("fchfin").value;

  var vcod = expresiones.codigo.test(cod);
  var vmar = expresiones.marca.test(mar);
  var vmod = expresiones.modelo.test(mod);
  var vaño = expresiones.año.test(año);

  if (vcod == true) {
    if (vmar == true) {
      if (vmod == true) {
        if (vaño == true) {
          if (fchini != "" && fchfin != "") {
            if (fchfin > fchini) {
              alert("Formulario exitoso");
            }else{
              alert("La fecha de inicio debe ser menor a la final");
            }
          } else {
            alert("Fechas incorrectas");
          }
        } else {
          alert("Año incorrecto");
        }
      } else {
        alert("Modelo incorrecto");
      }
    } else {
      alert("Marca incorrecta");
    }
  } else {
    alert("Codido incorrecto");
  }
}
